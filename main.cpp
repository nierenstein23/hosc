#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

#include "tausworthe.h"

#undef RAND_MAX
#define RAND_MAX 100

#define SQR(x) (x*x)

// oscillator properties
double omegasqr = 1.0, T=1.0, g = 0.0, l=0.0;
taus_state rndstate;

double dPhi(double phi, double pi, double epsilon) { return pi*epsilon; };
double dPi(double phi, double pi, double epsilon) { return -((omegasqr + (l/6.0)*SQR(phi))*phi + g*pi)*epsilon + sqrt(2.0*T*g*epsilon)*gaussiandouble(&rndstate); };
double H(double phi, double pi) { return SQR(pi)/2.0 + omegasqr*SQR(phi)/2.0 + (l/24.0)*SQR(SQR(phi)); };

void usage() {
    printf("\
        (An-)harmonic Oscillator. Writes to stdout in format 't phi pi e'.\n\
            \n\
            Options:\n\
            -h:\tDisplay this help message.\n\
            -g:\tLangevin Coupling\n\
            -w:\tOscillator Omega\n\
            -l:\tAnharmonicity\n\
            -n:\tNumber of Intervals\n\
            -s:\tSteps per Interval\n\
            -t:\tReal-Time Length of Interval\n\
            -T:\tTemperature\n\n");
}

int main(int argc, char **argv) {

    srand48(clock());
    rndstate = taus_seed();

    int c, N=0, s=1;
    double dt=1.0;

    while ((c = getopt (argc, argv, "g:w:l:n:s:t:T:h")) != -1) {
        switch (c) {
            case 'g': g = atof(optarg); break;
            case 'w': omegasqr = atof(optarg); break;
            case 'l': l = atof(optarg); break;
            case 'n': N = atoi(optarg); break;
            case 's': s = atoi(optarg); break;
            case 't': dt = atof(optarg); break;
            case 'T': T = atof(optarg); break;
            case 'h': usage(); return 0; break;
        }
    }


    double phi=gaussiandouble(&rndstate)*sqrt(T/sqrt(omegasqr*omegasqr)),
           pi=gaussiandouble(&rndstate)*sqrt(T);

    double epsilon = dt/s;

    printf("%.3e %.6e %.6e %.6e\n", 0.0, phi, pi, H(phi,pi));
    for (int i=0;i<N;i++) {
        // first half-step
        phi += dPhi(phi,pi,epsilon/2.0);

        for (int k=1;k<s;k++) {
            pi += dPi(phi,pi,epsilon);
            phi += dPhi(phi,pi,epsilon);
        }

        //last half-step
        pi += dPi(phi,pi,epsilon);
        phi += dPhi(phi,pi,epsilon/2.0);

        printf("%.6e %.12e %.12e %.12e\n", (i+1)*dt, phi, pi, H(phi,pi));
    }


    return 0;

}
