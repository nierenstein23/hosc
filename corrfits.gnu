#!/usr/bin/gnuplot -persist
#
# Plot stuff vs. functions
phitphi0(t)=C*exp(-g/2*abs(t))*(cos(w*t) + g/(2*w)*sin(w*abs(t)))
phitpi0(t)=exp(-g/2*abs(t))*sin(w*t)
pitpi0(t)=exp(-g/2*abs(t))*(-g/w*sin(w*abs(t)) +cos(w*t))
hth0(x)=(pitpi0(x)**2 + 2*phitpi0(x)**2 + phitphi0(x)**2)/4

w=sqrt(1.0-.01/4) # omega-tilde
g=0.1
C=1.0

pfix='s2e3'

set samples 1e3
set xr [-50:50]
set style data yerrorlines
set fit quiet

# phitphi0
fname=pfix.'phitphi0.corr'
cphiphi=system("grep -e '^0' ".fname." | cut -d' ' -f2")
fit [-1000:1000] phitphi0(x) fname u 1:($2/cphiphi):(sqrt($3)/(8*cphiphi)) zerr via C,g,w

set term epslatex size 14.5cm,10cm
set output 'phitphi0.tex'

set multiplot layout 2,1
set lmargin at screen .05
set bmargin at screen .25

set ytics .5
set xtics format ""

set label 1 at graph 0.15,0.85 sprintf('$\gamma=%.4f, \tilde \omega=%.4f, C=%.4f$', g,w,C)
p fname u 1:($2/cphiphi):(sqrt($3)/(8*cphiphi)) t '$\braket{\varphi(t)\varphi(0)$', phitphi0(x) t 'fit'

unset label 1

set tmargin at screen .25
set bmargin at screen .05

set ytics .005; set yr [-.009:.009]
set xtics format "%h"

unset key

p fname u 1:($2/cphiphi - phitphi0($1)):(sqrt($3)/(8*cphiphi)), 0

unset multiplot
set o
