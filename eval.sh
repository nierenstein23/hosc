#!/bin/bash

if [ -z ${NUM_THREADS+x} ]; then NUM_THREADS=$(nproc); fi

# give data directory as parameter
data=$1

# prefix for eval files as second parameter
prefix=$2

resultdir=$2

# data files generated have following structure:
# time phi pi H
#
# compute correlations:
# 1. <phi(t)phi(0)>
# 2. <pi(t)pi(0)>
# 3. <pi(t)phi(0)>
# 4. <pi^2(t)pi^2(0)>
# 5. <phi^2(t)phi^2(0)>
# 6. <pi^2(t)phi^2(0)>
# 7. <H(t)H(0)>
# 8. <pi^2(t)H(0)>
# 9. <phi^2(t)H(0)>

tmpdir=`mktemp -d`


for file in $data/osc*.dat; do
	./eval.py $file $prefix $tmpdir&
	if [[ "$(( counter += 1 ))" -ge "$NUM_THREADS" ]]; then wait; counter=0; fi
done

suffices=(ff fp pp ff2 fp2 pp2 efp ee)

for suf in ${suffices[@]}; do
	(
	raw_file=${resultdir}/${suf}.dat
	dyncombinator.py -a -i $tmpdir -s _${suf}.npz -o $raw_file

	n=$(cat $raw_file | wc -l)
	head -n $(( n/2 )) $raw_file | logbin.awk -v BinWidth="$BINWIDTH" | sort -g > ${resultdir}/${suf}_w.dat
	tail -n $(( n/2-1 )) $raw_file | logbin.awk -v BinWidth="$BINWIDTH" | sort -g > ${resultdir}/${suf}_t.dat
	)&
	if [[ "$(( counter += 1 ))" -ge "$NUM_THREADS" ]]; then wait; counter=0; fi
done
